# GCP Challange TF

## Name
Terraform LAB Challent for GCP PCA Certification setup structure

## Description

This project was created to minimize time to pass the next GCP Certification LAB Challenge:

https://partner.cloudskillsboost.google/focuses/16515

## Install and configure

### Install

```
cd
mkdir -p GCP
cd GCP
git clone git@gitlab.com:as190575/gcp-challange-tf.git
```

### Configure

Replace all occurencies of "CHANGE IT" to appropriate values from you GCP Lab settings.

## License
MIT License.

## Project status
Closed.
