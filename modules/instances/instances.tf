resource "google_compute_instance" "tf-instance-1" {
  name         = var.vm1_name
  machine_type = "n1-standard-1"
  zone         = var.zone_name

  network_interface {
    network	= "default"
  }

  boot_disk {
    initialize_params {
      image = var.image_name
    }
  }

  metadata_startup_script = <<-EOT
        #!/bin/bash
    EOT
  allow_stopping_for_update = true

}

resource "google_compute_instance" "tf-instance-2" {
  name         = var.vm1_name
  machine_type = "n1-standard-1"
  zone         = var.zone_name

  network_interface {
    network	= "default"
  }

  boot_disk {
    initialize_params {
      image = var.image_name
    }
  }

  metadata_startup_script = <<-EOT
        #!/bin/bash
    EOT
  allow_stopping_for_update = true

}

/*
resource "google_compute_instance" "CHANGE_IT" {
  name         = "CHANGE IT"
  machine_type = "n1-standard-2"
  zone         = var.zone_name

  boot_disk {
    initialize_params {
      image = var.image_name
  }

  network_interface {
    network	= "default"
  }

  metadata_startup_script = <<-EOT
        #!/bin/bash
    EOT
  allow_stopping_for_update = true

}
*/