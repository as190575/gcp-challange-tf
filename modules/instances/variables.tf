variable "vm1_name" {
  default	= "tf-instance-1"
}

variable "vm2_name" {
  default	= "tf-instance-2"
}

variable "zone_name" {
  default	= "CHANGE IT"
}

variable "image_name" {
  default	= "debian-10-buster-v20221206"
}